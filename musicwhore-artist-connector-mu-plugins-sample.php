<?php
/**
 * Musicwhore.org Artist Connector
 *
 * @package VigilantMedia\WordPress\Plugins\MusicwhoreOrg\ArtistConnector
 * @author Greg Bueno
 * @copyright 2016 Greg Bueno
 */

/*
 * This file is available for must-use plugin installation.
 *
 * 1. Install this plugin in the wp-content/mu-plugins directory.
 * 2. Copy or move this file to the root level of the mu-plugins directory.
 * 3. Rename this file to musicwhore-artist-connector.php.
 */

require WPMU_PLUGIN_DIR.'/musicwhore-artist-connector/musicwhore-artist-connector.php';